# PF2E Rule Element Generator

An easy way to generate rule elements for the Pathfinder 2e system. 
Click the button in an item's rules tab to access it (you must have Advanced Rule Element UI enabled in System Settings to see the tab). 
To learn more about rule elements go to https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/wikis/Going-Deeper-Topics/Quickstart-guide-for-rule-elements

### Manual Install

In Foundry setup, click on the Install Module button and put the following path in the Manifest URL.

`https://gitlab.com/pearcebasmanm/rule-element-generator/-/raw/main/module.json`

### Macro

To access the rule element generator outside of the rules tab, create a macro with the following code:

`new Application({
    width: 750,
    height: 600,
    popOut: true,
    minimizable: true,
    resizable: false,
    id: "rule-element-generator",
    template: "modules/rule-element-generator/application.html",
    title: "Rule Element Generator",
}).render(true);`